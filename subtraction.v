Load addition.

Theorem sub_ident: forall a, a - 0 = a.
  intros.

  induction a.

  (* a = 0 *)
  trivial.

  (* a ≠ 0 *)
  intros.
  trivial.
Qed.

Theorem sub_inverse: forall a, a - a = 0.
  intros.

  induction a.

  (* a = 0 *)
  trivial.

  (* a ≠ 0 *)
  trivial.
Qed.

Theorem sub_both_sides: forall a b c, a = b -> a - c = b - c.
  intros.
  rewrite <- H.
  trivial.
Qed.
