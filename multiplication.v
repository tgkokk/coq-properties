Load addition.

Theorem add_assoc': forall a b c, a + (b + c) = b + (a + c).
  intros.
  elim a.

  (* a = 0 *)
  trivial.

  (* a ≠ 0 *)
  intros.
  simpl.

  rewrite -> H.

  pose proof add_comm.

  symmetry.
  rewrite -> H0.
  simpl.
  
  symmetry.
  rewrite -> H0.
  trivial.
Qed.

Theorem mult_comm: forall a b, a * b = b * a.
  intros.

  elim a.

  (* a = 0 *)
  elim b.

  (* b = 0 *)
  trivial.

  (* b ≠ 0 *)
  intros.
  simpl.
  rewrite <- H.
  trivial.

  (* a ≠ 0 *)
  intros.
  simpl.
  rewrite -> H.

  elim b.

  (* b = 0 *)
  trivial.

  (* b ≠ 0 *)
  intros.
  simpl.
  rewrite <- H0.

  pose proof add_assoc'.

  rewrite -> H1.
  trivial.
Qed.

Theorem mult_ident: forall a, a * 1 = a.
  intros.

  pose proof mult_comm.
  rewrite -> H.
  simpl.

  apply add_ident.
Qed.

Theorem mult_dist: forall a b c, a * (b + c) = a * b + a * c.
  intros.

  elim a.

  (* a = 0 *)
  trivial.

  (* a ≠ 0 *)
  intros.
  simpl.
  rewrite -> H.

  elim b.

  (* b = 0 *)
  simpl.

  pose proof add_assoc'.
  rewrite -> H0.
  trivial.

  (* b ≠ 0 *)
  intros.
  simpl.

  assert (forall a b c d, a + b + (c + d) = a + c + (b + d)).
  intros.
  elim a0.

  (* a0 = 0 *)
  simpl.
  pose proof add_assoc'.
  rewrite -> H1.
  trivial.

  (* a0 ≠ 0 *)
  intros.
  simpl.
  rewrite -> H1.
  trivial.

  rewrite -> H1.
  trivial.
Qed.

Theorem mult_assoc: forall a b c, (a * b) * c = a * (b * c).
  intros.

  elim a.
  
  (* a = 0 *)
  trivial.

  (* a ≠ 0 *)
  intros.
  simpl.
  rewrite <- H.

  pose proof add_comm.
  pose proof mult_comm.
  pose proof mult_dist.

  rewrite -> H1.
  rewrite -> H2.

  symmetry.
  rewrite -> H0.
  rewrite -> H1.
  rewrite -> H0.
  rewrite -> H1.
  
  trivial.
Qed.
