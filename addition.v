Load subtraction.

Theorem add_comm: forall a b, a + b = b + a.
  intros.
  elim a.

  (* a = 0 *)
  elim b.

  (* b = 0 *)
  trivial.

  (* b ≠ 0 *)
  intros.
  simpl.
  rewrite <- H.
  trivial.

  (* a ≠ 0 *)
  intros.
  simpl.
  rewrite -> H.

  elim b.

  (* b = 0 *)
  trivial.

  (* b ≠ 0 *)
  intros.
  simpl.
  rewrite -> H0.
  trivial.
Qed.

Theorem add_assoc: forall a b c, (a + b) + c = a + (b + c).
  intros.

  elim a.

  (* a = 0 *)
  simpl.
  trivial.

  (* a ≠ 0 *)
  intros.
  simpl.
  rewrite -> H.
  trivial.
Qed.

Theorem add_ident: forall a, a + 0 = a.
  intros.
  pose proof add_comm.
  rewrite -> H.
  trivial.
Qed.

Theorem add_both_sides: forall a b c, a = b -> a + c = b + c.
  intros.
  rewrite <- H.
  trivial.
Qed.

Theorem add_both_sides_rev: forall a b c, a + c = b + c -> a = b.
  pose proof sub_both_sides.
  pose proof add_comm.

  assert (forall a b, b + a - b = b - b + a).
  intros.
  induction b.

  (* b0 = 0 *)
  simpl.
  pose proof sub_ident.
  rewrite -> H2.
  trivial.

  (* b0 ≠ 0 *)
  trivial.

  intros.
  apply (H0 _ _ c) in H3.

  rewrite -> H1 in H3.
  rewrite -> H2 in H3.
  rewrite -> H in H3.
  symmetry in H3.
  rewrite -> H1 in H3.
  rewrite -> H2 in H3.
  rewrite -> H in H3.
  symmetry in H3.

  simpl in H3.
  trivial.
Qed.
